package com.example.gomaa.myapplication;

import android.telecom.Call;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.GET;

public interface IApi {
    @GET("/posts")
    retrofit2.Call<List<JsonData>> getData();
}
