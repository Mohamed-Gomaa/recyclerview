package com.example.gomaa.myapplication;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;


public class SongAdapter extends RecyclerView.Adapter<SongAdapter.SongViewHolder> implements View.OnClickListener
{

    LayoutInflater layoutInflater;

    List<JsonData> data;
    public SongAdapter(List<JsonData> data)
    {
        this.data=data;
    }

    @NonNull
    @Override
    public SongViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.song_single_item,parent);
        return new SongViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SongViewHolder holder, int position) {

        holder.song_name.setText(data.get(position).getTitle());
        holder.song_duration.setText(data.get(position).getBody());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public void onClick(View view) {

    }

    public class SongViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.song_name)
        TextView song_name;
        @BindView(R.id.song_duration)
        TextView song_duration;
        @BindView(R.id.img_item)
        ImageView img_item;

        public SongViewHolder(View itemView) {
            super(itemView);

        }
    }
}
