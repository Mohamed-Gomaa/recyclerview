package com.example.gomaa.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

     @BindView(R.id.song_recycle_view)
     RecyclerView song_recycle_view;
     private RecyclerView.LayoutManager layoutManager;
     private SongAdapter songAdapter;
     private List<ApiClient> data;
     private IApi api;
     


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        layoutManager=new LinearLayoutManager(this);
        song_recycle_view.setLayoutManager(layoutManager);
        song_recycle_view.setHasFixedSize(true);
        api=ApiClient.getApiClient().create(IApi.class);

        Call<List<JsonData>> call=api.getData();
        call.enqueue(new Callback<List<JsonData>>() {
            @Override
            public void onResponse(Call<List<JsonData>> call, Response<List<JsonData>> response) {
                data = response.body();
                songAdapter = new SongAdapter(JsonData);
                song_recycle_view.setAdapter(songAdapter);
            }
            @Override
            public void onFailure(Call<List<JsonData>> call, Throwable t) {

            }
        })
            @Override
            public void onFailure(Call<List<JsonData>> call, Throwable t) {

            }
        });
        

    }
}
